// Mundo.cpp: implementation of the CMundo class.
// Modificacion ADRIANGDENIA
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	pDatos->cerrar=1;
	close(fd2);
	
	munmap(pDatos,sizeof(DatosMemCompartida));
	if(unlink("/tmp/MemoriaB")<0)
	{
	perror("Error al desenlazar Mem_B");
	exit(1);
	}
	sock_cs.Close();
	
	exit(0);
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
		


	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	
	
	char cad[200];
	if(sock_cs.Receive(cad,sizeof(cad))<0)
	{
	perror("Error al leer coordenadas");
	exit(1);
	}
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d",&esfera.radio, &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	
	if(puntos1>2||puntos2>2) OnKeyboardDown('f',0,0);
	
	pDatos->raqueta1=jugador1;
	pDatos->raqueta2=jugador2;
	pDatos->esfera=esfera;


	if(pDatos->accion1==1)
	OnKeyboardDown('w', 0, 0);
	else if (pDatos->accion1==-1)
	
	OnKeyboardDown('s', 0, 0);
	if(pDatos->accion2==1){
	//OnKeyboardDown('o', 0, 0);
	}
	else if (pDatos->accion2==-1){
	//OnKeyboardDown('l', 0, 0);
	
	}
	
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char buf[10];
	sprintf(buf,"%c",key);
	if(sock_cs.Send(buf,sizeof(buf))<0)
	{
	perror("Error al escribir teclas");
	exit(1);
	}
	if(key=='f')
	this->~CMundo();

	
}

void CMundo::Init()
{
	if(sock_cs.Connect("127.0.0.1",8080)<0) //Cambiar IP por la IP del servidor al que se conecte 
	{
	perror("Error al conectar al server");
	exit(1);
	}
	write(1, "Nombre: ", sizeof("Nombre: "));
	char nombre[200];
	read(0,nombre,sizeof(nombre));
	if(sock_cs.Send(nombre,strlen(nombre))<0)
	{
	perror("Error al enviar nombre");
	exit(1);
	}
	
	Datos.raqueta1=jugador1;
	Datos.raqueta2=jugador2;
	Datos.esfera=esfera;
	Datos.accion1=0;	
	Datos.accion2=0;
	Datos.cerrar=0;
		
	
	if((fd2=open("/tmp/MemoriaB",O_CREAT|O_TRUNC|O_RDWR,0666))<0)
	{
	perror("Error al abrir Fich_Mem_Comp_Bot");
	exit(1);
	}
	write(fd2, &Datos,sizeof(DatosMemCompartida));
	pDatos=(DatosMemCompartida*)mmap(0,sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2,0);
	close(fd2);
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

}



