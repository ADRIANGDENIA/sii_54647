// Mundo.cpp: implementation of the CMundo class.
// Modificacion ADRIANGDENIA
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	pthread_join(thid1,NULL);
	close(fd);
	sock_cs.Close();
	exit(0);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
		


	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	
	if(!fin)
	{
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		char aux[2];
		aux[0]='2';
		aux[1]=puntos2+48;
		if(write(fd,&aux,sizeof(aux))<0)
		{
		perror("Error al escribir Serv_Log");
		exit(1);
		}
		AvanceJuego();
		
	}

	if(fondo_dcho.Rebota(esfera))
	{	
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		char aux1[2];
		aux1[0]='1';
		aux1[1]=puntos1+48;
		if(write(fd,&aux1,sizeof(aux1))<0)
		{
		perror("Error al escribir Serv_Log");
		exit(1);
		}
		AvanceJuego();
	}
	
	}
		//Enviar datos al cliente
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d",esfera.radio, esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	
	if(sock_cs.Send(cad,sizeof(cad))<0)
	{
		perror("Error al escribir coordenadas");
		exit(1);
	}
	
	
	
}

void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
  char cad[100];
  int K;
     while ((K=(sock_cs.Receive(cad,sizeof(cad))))!=-1) {
            usleep(10);          
            unsigned char key;
            sscanf(cad,"%c",&key);
            switch(key)
		{
		case 'p': fin=0; break;
		//case 'a':jugador1.velocidad.x=-1;break;
		//case 'd':jugador1.velocidad.x=1;break;
		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;
		case 'f':this->~CMundo(); break;
		}
      }
          if(K<0)
            {
            perror("Error al leer teclas");
            exit(1);
            }
}

void CMundo::AvanceJuego()
{
		
	
	switch(puntos2)
	{
		case 0:  break;
		case 1:  esfera.radio=0.3;  break;
		case 2:  esfera.radio=0.15; break;
		
		default: break;
	}
	switch(puntos1)
	{
		case 0:  break;
		case 1:  jugador1.y1=-0.6; jugador1.y2=0.6; break;
		case 2:  jugador1.y1=-0.4; jugador1.y2=0.4; break;
	
		default: break;
	}
	
	

}
void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	
}

void CMundo::Init()
{

	fin =1;
	
	if(sock_tcp.InitServer("127.0.0.1",8080)<0)
	{
	perror("Error al iniciar server");
	exit(1);
	}
	
	sock_cs=sock_tcp.Accept();
	char nombre[100];
	if(sock_cs.Receive(nombre, strlen(nombre))<0)
	{
	perror("Error al recibir nombre");
	exit(1);
	}
	else
	{
	std::cout<<"Nombre cliente '"<<nombre<<"'"<<std::endl;
	}
	
	pthread_create(&thid1, NULL, hilo_comandos, this);
	
	if((fd=open("/tmp/TuberiaL",O_WRONLY))<0)
	{
	perror("Error al abrir FIFO_Log");
	exit(1);
	}
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	write(1, "Pulsa P para comenzar\n", sizeof("Pulsa P para comenzar\n"));
}


