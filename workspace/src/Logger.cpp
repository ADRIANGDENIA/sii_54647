#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <iostream>

int main()
{
	
	int fd;
	char buf[2];
	int puntos=0;
	
	
	if(mkfifo("/tmp/TuberiaL",0666)<0){
	perror("Error crear FIFO_Log");
	exit(1);
	}
	if((fd=open("/tmp/TuberiaL", O_RDONLY))<0){
	perror("Error abrir FIFO_Log");
	exit(1);
	}


	while(read(fd,&buf,sizeof(buf))==sizeof(buf)){
		if(buf[1]-48<3)
		std::cout<<"Jugador "<<buf[0]<<" marca 1 lleva un total de "<<buf[1]<<"\n";
		else
		std::cout<<"Jugador "<<buf[0]<<" Ha ganado"<<"\n";
		
	}
	std::cout<<"Fin del juego\n";
	close(fd);
	if(unlink("/tmp/TuberiaL")<0)
	{
	perror("Error al desenlazar FIFO_Log");
	}
	exit(1);
}
